<?php
/**
 * Created by PhpStorm.
 * User: milan
 * Date: 5.10.19.
 * Time: 16.43
 */

$factory->define(\App\Models\Thread::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->title,
        'description' => $faker->paragraph(3),
        'user_id' => factory(\App\Models\User::class)->create()->id,
    ];
});