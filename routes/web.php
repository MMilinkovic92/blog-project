<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    dd(route('threads.post.create'));
    return $router->app->version();
});

$router->post('/registration', ["as" => "registration", "uses" => "AuthController@register"]);
$router->post('/auth/login', ["as" => "login",  "uses" => "AuthController@authenticate"]);


$router->group(['middleware' => 'jwt.auth'], function() use ($router) {
    $router->get('test/login', function () {return "success";});
});


$router->group(['middleware' => 'jwt.auth', "prefix" => "threads"], function() use ($router) {

    $router->get('/', ["as" => "threads.index", "uses" => "ThreadController@index"]);

    //create
    $router->get('create', ["as" => "threads.get.create", "uses" => "ThreadController@getCreate"]);
    $router->post('create', ["as" => "threads.post.create", "uses" => "ThreadController@postCreate"]);

    //update
    $router->get('edit/{id}', ["as" => "threads.get.edit", "uses" => "ThreadController@getEdit"]);
    $router->put('edit/{id}', ["as" => "threads.post.edit", "uses" => "ThreadController@postEdit"]);

    //delete
    $router->delete('delete/{id}', ["as" => "threads.delete", "uses" => "ThreadController@postDelete"]);
});