<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'phone_number',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];


    /**
     * This method returns default password for user, mostly used for testing.
     *
     * @return string
     */
    public function getDefaultPassword() : string
    {
        return "12345";
    }

    /**
     * Get the threads for the user.
     *
     * @return HasMany
     */
    public function threads() : HasMany
    {
        return $this->hasMany(Thread::class, 'user_id', 'id');
    }

    /**
     * Check does specific thread is owned by this user.
     *
     * @param int $id
     * @return bool
     */
    public function hasSpecificThread(int $id) :  bool
    {
        return $this->threads()->pluck("id")->contains($id);
    }
}
