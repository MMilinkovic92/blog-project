<?php
/**
 * Created by PhpStorm.
 * User: milan
 * Date: 5.10.19.
 * Time: 19.27
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'user_id', 'comment_id'
    ];

}