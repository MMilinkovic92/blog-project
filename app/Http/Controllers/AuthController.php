<?php
/**
 * Created by PhpStorm.
 * User: milan
 * Date: 5.10.19.
 * Time: 12.16
 */

namespace App\Http\Controllers;


use App\Models\User;
use App\Services\User\UserRegistrationService;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * @var Request
     */
    private $request;


    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Create a new token.
     *
     * @param  User   $user
     * @return string
     */
    protected function jwt(User $user) : string
    {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60*60 // Expiration time
        ];

        return JWT::encode($payload, env('JWT_SECRET'));
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     *
     * @return mixed
     */
    public function authenticate()
    {
        $this->validate($this->request, ['email' => 'required|email', 'password'  => 'required']);
        // Find the user by email
        $user = User::where('email', $this->request->get('email'))->first();

        if (!$user) {
            return response()->json([
                'error' => 'Email does not exist.'
            ], 400);
        }

        // Verify the password and generate the token
        if (Hash::check($this->request->input('password'), $user->password)) {
            return response()->json([
                'token' => $this->jwt($user)
            ], 200);
        }

        // Bad Request response
        return response()->json([
            'error' => 'Email or password is wrong.'
        ], 400);
    }

    public function register(UserRegistrationService $registrationService)
    {
        $this->validate($this->request,
                        [
                            'first_name' => ['required'],
                            'last_name' => ['required'],
                            'email' => ['required', 'email', 'unique:users'],
                            'password' => ['required'],
                            'phone_number' => ['required', 'unique:users']
                        ]);

        return $registrationService->register($this->request->all());
    }

}