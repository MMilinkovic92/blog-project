<?php
/**
 * Created by PhpStorm.
 * User: milan
 * Date: 5.10.19.
 * Time: 16.50
 */

namespace App\Http\Controllers;


use App\Library\AuthHelper;
use App\Models\Thread;
use App\Models\User;
use App\Services\Thread\ThreadCreateService;
use App\Services\Thread\ThreadDeleteService;
use App\Services\Thread\ThreadEditService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ThreadController extends Controller
{

    protected $request;
    protected $user;
    protected $threadEditService;
    protected $threadDeleteService;

    public function __construct(Request $request, ThreadEditService $threadEditService, ThreadDeleteService $threadDeleteService)
    {
        $this->request = $request;
        $this->user = AuthHelper::getAuthUser($this->request->header('token'));
        $this->threadEditService = $threadEditService;
        $this->threadDeleteService = $threadDeleteService;
    }

    /**
     * Return all threads.
     *
     * @return JsonResponse
     */
    public function index() : JsonResponse
    {
        return response()->json(Thread::all());
    }

    /**
     * Necessary data for Thread creation.
     */
    public function getCreate()
    {
        //Ovde mogu da se vrate neke labele, lokalizovane stvari za neku drzavu, flagovi sta je od polja sakriveno i slicno
    }

    /**
     * Create Thread.
     *
     * @param ThreadCreateService $threadCreateService
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postCreate(ThreadCreateService $threadCreateService) : JsonResponse
    {
        $this->validate(
                $this->request,
                [
                    'title' => ['required'],
                    'description' => ['required'],
                    'image' => ['mimes:jpg,png']
                ]);

        return $threadCreateService->create($this->request->all(), $this->user);
    }

    /**
     * Update specific thread.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function getEdit(int $id) : JsonResponse
    {
        try{
            $thread = Thread::findOrFail($id);
        } catch (\Exception $exception)
        {
            Log::error($exception->getMessage());
            return response()->json(['error' => 'Could not find Thread']);
        }

        return $this->threadEditService->edit($this->request->all(), $thread, $this->user);
    }

    /**
     * Delete specific thread.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function postDelete(int $id) : JsonResponse
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (\Exception $exception)
        {
            Log::error($exception->getMessage());
            return response()->json(['error' => 'Could not find Thread']);
        }

        return $this->threadDeleteService->delete($thread);
    }


}