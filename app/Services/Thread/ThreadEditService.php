<?php
/**
 * Created by PhpStorm.
 * User: milan
 * Date: 5.10.19.
 * Time: 18.35
 */

namespace App\Services\Thread;

use App\Models\Thread;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class ThreadEditService
{

    /**
     * Update specific thread.
     *
     * @param array $data
     * @param Thread $thread
     * @param User $user
     * @return JsonResponse
     */
    public function edit(array $data, Thread $thread, User $user) : JsonResponse
    {
        if($this->validate($thread, $user)) {
            return $this->updateThread($data, $thread);
        } else {
            return response()->json(['error' => 'This user can not edit this thread']);
        }
    }

    /**
     * Update thread in database.
     *
     * @param array $data
     * @param Thread $thread
     * @return JsonResponse
     */
    private function updateThread(array $data, Thread $thread) : JsonResponse
    {
        try{
            $thread->update($data);
            return response()->json(['success' => 'This thread is updated.']);
        } catch (\Exception $exception)
        {
            Log::error($exception->getMessage());
            return response()->json(['error' => 'There is an error during updating thread.']);
        }
    }


    /**
     * Validate update execution for specific thread.
     *
     * @param $thread
     * @param $user
     * @return bool
     */
    private function validate($thread, $user) : bool
    {
        return $user->hasSpecificThread($thread->id) && $thread->canBeEdited();
    }

}