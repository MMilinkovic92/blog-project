<?php
/**
 * Created by PhpStorm.
 * User: milan
 * Date: 5.10.19.
 * Time: 19.07
 */

namespace App\Services\Thread;


use App\Models\Thread;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class ThreadDeleteService
{

    /**
     * Delete thread from database.
     *
     * @param Thread $thread
     * @return JsonResponse
     */
    public function delete(Thread $thread) : JsonResponse
    {
        //Moze da se proveri da li postoji slika ili neki drugi zavisni entitet i da se obrise
        // ako ne radimo kaskadno brisanje u bazi zbog nekog dogovora
        try{
            $thread->delete();
            return response()->json(['success', 'Successful deleting of thread.']);
        } catch (\Exception $exception)
        {
            Log::error($exception->getMessage());
            return response()->json(['error' => 'Unsuccessful deleting of thread']);
        }

    }

}