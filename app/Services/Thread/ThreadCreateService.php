<?php
/**
 * Created by PhpStorm.
 * User: milan
 * Date: 5.10.19.
 * Time: 17.30
 */

namespace App\Services\Thread;


use App\Models\Thread;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class ThreadCreateService
{

    /**
     * @param array $data
     * @param User $user
     * @return JsonResponse
     */
    public function create(array $data, User $user) : JsonResponse
    {
        //It's useful to have User instance in this Service when it grows bigger
        $data['user_id'] = $user->id;

        try{
            Thread::create($data);
        }catch (\Exception $exp) {
            Log::error($exp->getMessage());

            return response()->json(['error' => 'Unsuccessful create of thread.']);
        }

        return response()->json(['success' => 'Successful create of thread.']);
    }

}