<?php
/**
 * Created by PhpStorm.
 * User: milan
 * Date: 5.10.19.
 * Time: 14.02
 */

namespace App\Services\User;


use App\Models\User;
use Illuminate\Support\Facades\Log;

class UserRegistrationService
{

    public function register(array $data)
    {

        try{
            User::create($data);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());

            return response()->json([
                'error' => "User does not register successfully."
            ]);
        }

        return response()->json([
            'success' => 'User registered successfully.'
        ]);
    }


}