<?php
/**
 * Created by PhpStorm.
 * User: milan
 * Date: 5.10.19.
 * Time: 17.37
 */

namespace App\Library;


use App\Models\User;
use Firebase\JWT\JWT;

class AuthHelper
{

    /**
     * Get auth user from token.
     *
     * @param string $token
     * @return User
     */
    public static function getAuthUser(string $token) : User
    {
        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        $user = User::find($credentials->sub);

        return $user;
    }


}