<?php
/**
 * Created by PhpStorm.
 * User: milan
 * Date: 5.10.19.
 * Time: 14.20
 */

use App\Models\User;

class RegistrationTest extends TestCase
{
    /** @test */
    public function it_checks_user_successful_registration()
    {
        //register user
        $response = $this->post(route("registration"),
            [
                "first_name" => "Pera",
                "last_name"  => "Peric",
                "email" => "pera@yopmail.com",
                "password" => "12345",
                "phone_number" => "1235425234"
            ])
            ->response
            ->getOriginalContent()['success'];

        $this->assertTrue($this->userExists("pera@yopmail.com"));
        $this->assertEquals($response, "User registered successfully.");
    }

    /** @test */
    public function it_checks_user_unsuccessful_registration_attempt()
    {
        //idea is to sql fails because phone_number is to long
        $response = $this->post(route("registration"),
            [
                "first_name" => "Pera",
                "last_name"  => "Peric",
                "email" => "pera@yopmail.com",
                "password" => "12345",
                "phone_number" => "123542523asdasdasdasdasdasdasda4"
            ])
            ->response
            ->getOriginalContent()['error'];

        $this->assertFalse($this->userExists("pera@yopmail.com"));
        $this->assertEquals($response, "User does not register successfully.");
    }

    public function userExists(string $email) : bool
    {
        return !is_null(User::where('email', $email)->first());
    }
}