<?php
/**
 * Created by PhpStorm.
 * User: milan
 * Date: 5.10.19.
 * Time: 13.07
 */

class LoginTest extends TestCase
{
    /** @test */
    public function it_checks_user_login()
    {
        //create new user
        $user = factory(\App\Models\User::class)->create();

        //login user
        $token = $this->post(route("login"),
                            ["email" => $user->email, "password" => $user->getDefaultPassword()])
                            ->response
                            ->getOriginalContent()['token'];

        //verify login
        $test = $this->get('http://project.test/test/login', ['token' => $token])->response->getContent();

        $this->assertEquals($test, "success");
    }

    /** @test */
    public function it_checks_unauthorized_attempt()
    {
        //verify login
        $test = $this->get('http://project.test/test/login', ['token' => "123"])
                     ->response
                     ->getOriginalContent()['error'];

        $this->assertEquals($test, "An error while decoding token.");
    }

}